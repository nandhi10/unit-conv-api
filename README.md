# UnitConversion API

A NodeJs based UnitConversion API which will take a JSON payload and calculate metrics temperature/volume from one unnit to another unit.

---
## Requirements

For development, you will only need Node.js and npm on the machine.

If you need to update `npm`, you can make it using `npm`! Cool right? After running the following command, just open again the command line and be happy.

    $ npm install npm -g

## Installation
git clone git@gitlab.com:nandhi10/unit-conv-api.git

cd unit-conv-api

npm install

npm start

## Running API Tests
npm run test

## API Endpoints
POST /api/v1/unitconversion/temperature.

POST /api/v1/unitconversion/volume.

## Postman
Install Postman to interact with REST API

Import the collection from repository

## API USAGE

| Temperature | Scientific Unit | Acceptable format in JSON  |
| ------------- |:-------------:| -----:|
| | Kelvin | K |
| | Celsius | C |
| | Fahrenheit | F |

| Volume | Scientific Unit | Acceptable format in JSON |
| ------------- |:-------------:| -----:|
| | liters  | l |
| | tablespoons  | Tbs |
| | cubic-inches | in3 |
| | cups | cup |
| | cubic-feet | ft3 |
| | gallons | gal |

All other representation will result in 400 exception with invalid data.

## Future Development Scope
1) Build a Nextjs/Vue based UI
2) Integrate code coverage with sonarcloud
3) Expose a command prompt based solution (more of user interactive/reactive)
4) Automate Swagger document using jest/swagger-jsdoc
5) set up heroku deploy channel through gitlab pipeline

