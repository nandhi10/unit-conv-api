import express from "express";
import * as conversionController from "../controller/conversionController.js";
import logger from '../logger.js';

const router = express.Router();



/**
 * @swagger
 * /unitConversion/temperature
 *  post:
 *    description: Temp Conv API
 *  responses:
 *     200:
 *          A success comparison response
 *     400:
 *          Invaild Input  - Please verify
 */
router.post("/temperature", (req, res) => {
    logger.debug('Request received for conversion  - '+ JSON.stringify(req.body)) 
    const response = conversionController.doTempConversion(req,res);
    if(response.authoritativeResponse.ERROR !== undefined){
        res.status(400).send(response);
    }else if(response.authoritativeResponse.EXCEPTION !== undefined){
        res.status(500).send(response);
    }else{
        res.send(response);
    }
});

/**
 * @swagger
 * /UnitConversion/volume:
 *  post:
 *  description - Unit Conversion API
 *  resposes:
 *      200:
 *       A success comparison response
 *      400:
 *       Invaild Input  - Please verify
 */
router.post("/volume", (req, res) => {
    logger.debug('Request received for conversion  - '+ JSON.stringify(req.body)) 
    const response = conversionController.doVolumeConversion(req);
    if(response.authoritativeResponse.ERROR !== undefined){
        res.status(400).send(response);
    }else if(response.authoritativeResponse.EXCEPTION !== undefined){
        res.status(500).send(response);
    }else{
        res.send(response);
    }
});

export default router;
