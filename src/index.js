import express from 'express';
import bodyparser from 'body-parser';
import convRoutes from './routes/conversionRoute.js';
import config from 'config'
import swaggerUi from 'swagger-ui-express';

import swaggerDoc from './swagger.js';

//define express appp.
const app = express();
const PORT = config.get('appPort');
const BASEPATH = config.get('basePath');

app.use(bodyparser.json());
//define route paths
app.use(`${BASEPATH}/unitconversion`, convRoutes);
app.use(`${BASEPATH}/health`, (req, res) => {
    res.send('Health Check - Complete');
})
app.use(`${BASEPATH}/doc`, (req, res) => {
    res.send('Doc Route');
})
app.use(`${BASEPATH}/api-docs`, swaggerUi.serve, swaggerUi.setup(swaggerDoc));

app.listen(PORT, () => console.log(`UnitConv app is running and listening under port : http://localhost:${PORT}${BASEPATH}`));

export default app