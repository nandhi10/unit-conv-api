const unitconversion = {
    tags: ['unitConversion'],
    description: "Converts Temperature from one unit to another and validates",
    operationId: 'tempconv',
    //security: [
    //  {
    //    bearerAuth: []
    //}
    //],
    parameters: [],
    requestBody: {
        content: {
            "*/*": {
                schema: {
                    unitconversion: {
                        type: 'object',
                        properties: {
                            inputValue: {
                                type: 'string',
                                description: 'input value to be converted'
                            },
                            inputUnit: {
                                type: 'string',
                                description: 'Measurement unit of input value'
                            },
                            outputUnit: {
                                type: 'string',
                                description: 'Measurement unit of output value'
                            },
                            studentResponse: {
                                type: 'string',
                                description: 'Student Calcaulated Value - To be validated '
                            }
                        }


                    }
                }
            }
        },
        required: true
    },
    responses: {
        "200": {
            description: "Converted value of Temperature and authoritative validation of user calcualtion",
            "content": {
                "application/json": {
                    schema: {
                        unitconversion: {
                            type: 'object',
                            inputValue: {
                                type: 'string',
                                description: 'input value to be converted'
                            },
                            inputUnit: {
                                type: 'string',
                                description: 'Measurement unit of input value'
                            },
                            outputUnit: {
                                type: 'string',
                                description: 'Measurement unit of output value'
                            },
                            studentResponse: {
                                type: 'string',
                                description: 'Student Calcaulated Value - To be validated '
                            },
                            authoritativeResponse: {
                                CalculatedOutput: {
                                    type: 'string',
                                    description: 'Authoritative Calcaulated Value '
                                },
                                conversionResult: {
                                    type: 'string',
                                    description: 'Authoritative Result against Student response value'
                                }

                            }



                        }
                    }
                }
            }
        }
    }
}

export default { unitconversion }