import log4js from 'log4js';
import config from 'config';

const FILENAME = config.get('logFileName');

log4js.configure({
    appenders: { fileLog: { type: "file", filename: `${FILENAME}` } },
    categories: { default: { appenders: ["fileLog"], level: "debug" } }
});

const logger = log4js.getLogger("fileLog");

export default logger