const swaggerDoc = {
    openapi: '3.0.1',
    swaggerDefinition: {
        info: {
            version: '1.0.0',
            title: 'UnitConv API Document',
            description: 'API to convert between units and validate your converted output',
            termsOfService: '',
            contact: {
                name: 'Nandhi'
            },
            license: {
                name: 'Apache 2.0',
                url: 'https://www.apache.org/licenses/LICENSE-2.0.html'
            }
        }
    },
    servers: [
        {
            "url": "https://localhost:{port}/{basePath}",
            "description": "UnitConversion API Dev work",
            "variables": {
                "env": {
                    "default": "app-dev",
                    "description": "DEV Environment"
                },
                "port": {
                    "enum": [
                        "5000"
                    ],
                    "default": "5000"
                },
                "basePath": {
                    "default": "api/v1"
                }
            }
        }],
    tags: [
        {
            name: 'unitConversion',
            "descrption": "Everything about unitconversion API"
        }
    ],
    paths: {
        "/unitConversion/temperature": {
            post: {
                tags: ['unitConversion'],
                description: "Converts Temperature from one unit to another and validates",
                operationId: 'tempconv',
                //security: [
                //  {
                //    bearerAuth: []
                //}
                //],
                parameters: [],
                requestBody: {
                    content: {
                        'application/json': {
                            schema: {
                                unitconversion: {
                                    type: 'object',
                                    properties: {
                                        inputValue: {
                                            type: 'string',
                                            description: 'input value to be converted'
                                        },
                                        inputUnit: {
                                            type: 'string',
                                            description: 'Measurement unit of input value'
                                        },
                                        outputUnit: {
                                            type: 'string',
                                            description: 'Measurement unit of output value'
                                        },
                                        studentResponse: {
                                            type: 'string',
                                            description: 'Student Calcaulated Value - To be validated '
                                        }
                                    }
                                }
                            }
                        }
                    },
                    required: true
                },
                responses: {
                    "200": {
                        description: "Converted value of Temperature and authoritative validation of user calcualtion",
                        "content": {
                            "application/json": {
                                schema: {
                                    unitconversionResponse: {
                                        type: 'object',
                                        properties: {
                                            inputValue: {
                                                type: 'string',
                                                description: 'input value to be converted'
                                            },
                                            inputUnit: {
                                                type: 'string',
                                                description: 'Measurement unit of input value'
                                            },
                                            outputUnit: {
                                                type: 'string',
                                                description: 'Measurement unit of output value'
                                            },
                                            studentResponse: {
                                                type: 'string',
                                                description: 'Student Calcaulated Value - To be validated '
                                            },
                                            authoritativeResponse: {
                                                CalculatedOutput: {
                                                    type: 'string',
                                                    description: 'Authoritative Calcaulated Value '
                                                },
                                                conversionResult: {
                                                    type: 'string',
                                                    description: 'Authoritative Result against Student response value'
                                                }

                                            }


                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },
        "/unitConversion/volume": {
            post: {
                tags: ['unitConversion'],
                description: "Converts Volume from one unit to another and validates",
                operationId: 'volumeConv',
                //security: [
                //  {
                //    bearerAuth: []
                //}
                //],
                parameters: [],
                requestBody: {
                    content: {
                        'application/json': {
                            schema: {
                                unitconversion: {
                                    type: 'object',
                                    properties: {
                                        inputValue: {
                                            type: 'string',
                                            description: 'input value to be converted'
                                        },
                                        inputUnit: {
                                            type: 'string',
                                            description: 'Measurement unit of input value'
                                        },
                                        outputUnit: {
                                            type: 'string',
                                            description: 'Measurement unit of output value'
                                        },
                                        studentResponse: {
                                            type: 'string',
                                            description: 'Student Calcaulated Value - To be validated '
                                        }
                                    }
                                }
                            }
                        }
                    },
                    required: true
                },
                responses: {
                    "200": {
                        description: "Converted value of Temperature and authoritative validation of user calcualtion",
                        "content": {
                            "application/json": {
                                schema: {
                                    unitconversionResponse: {
                                        type: 'object',
                                        properties: {
                                            inputValue: {
                                                type: 'string',
                                                description: 'input value to be converted'
                                            },
                                            inputUnit: {
                                                type: 'string',
                                                description: 'Measurement unit of input value'
                                            },
                                            outputUnit: {
                                                type: 'string',
                                                description: 'Measurement unit of output value'
                                            },
                                            studentResponse: {
                                                type: 'string',
                                                description: 'Student Calcaulated Value - To be validated '
                                            },
                                            authoritativeResponse: {
                                                CalculatedOutput: {
                                                    type: 'string',
                                                    description: 'Authoritative Calcaulated Value '
                                                },
                                                conversionResult: {
                                                    type: 'string',
                                                    description: 'Authoritative Result against Student response value'
                                                }

                                            }


                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}

export default swaggerDoc