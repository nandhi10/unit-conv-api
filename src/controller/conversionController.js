import * as conversion from "../service/conversion.js";
import config from "config";
import logger from '../logger.js'

const formatter = new Intl.NumberFormat("en-US", {
    minimumFractionDigits: 1,
    maximumFractionDigits: 1,
});

/**
 * Temperature Copnversion and Validation
 * @param {*} req 
 */

const doTempConversion = (req) => {
    const response = req.body;
    response["authoritativeResponse"] = {};
    try {
        if ((config.conversion.temperature.includes(req.body.inputUnit)) && (config.conversion.temperature.includes(req.body.outputUnit))) {

            //use convert-units node module to do authoritative conversion
            const calculatedOutput = formatter.format(conversion.convertUnit(req.body));

            logger.info(`Calcualted Output ${calculatedOutput} for Input ${req.body.inputValue} : Unit ${req.body.inputUnit}`)

            response["authoritativeResponse"]["CalculatedOutput"] = calculatedOutput;

            let conversionResult = 'Student Response IS IN-CORRECT';
            if (
                formatter.format(req.body.studentResponse) === calculatedOutput
            ) {
                logger.info('Unit Conversion is valid and correct');
                conversionResult = "Student Response IS CORRECT";
            }
            response["authoritativeResponse"]["conversionResult"] = conversionResult;
        } else {
            logger.warn("Request has Invalid Data ");
            response["authoritativeResponse"]["ERROR"] = `Request has Invalid Data .. Please verify agaist Service Documentation`
        }
    } catch (error) {
        logger.error(`Exception - Temperature Conversion ${error}`);
        response["authoritativeResponse"]["EXCEPTION"] = `Exception - Temperature Conversion - Please reach out to service owners`;
    }
    return response;
}

/**
 * Volume Copnversion and Validation
 * @param {*} req 
 */
const doVolumeConversion = (req) => {
    const response = req.body;
    response["authoritativeResponse"] = {};
    try {
        if ((config.conversion.volume.includes(req.body.inputUnit)) && config.conversion.volume.includes(req.body.outputUnit)) {

            //use convert-units node module to do authoritative conversion
            const calculatedOutput = formatter.format(conversion.convertUnit(req.body));

            logger.info(`Calcualted Output ${calculatedOutput} for Input ${req.body.inputValue} : Unit ${req.body.inputUnit}`)

            response["authoritativeResponse"]["CalculatedOutput"] = calculatedOutput;

            let conversionResult = 'Student Response IS IN-CORRECT';
            if (
                formatter.format(req.body.studentResponse) === calculatedOutput
            ) {
                logger.info('Unit Conversion is valid and correct');
                conversionResult = "Student Response IS CORRECT";
            }
            response["authoritativeResponse"]["conversionResult"] = conversionResult;
        } else {
            logger.warn("Volume Conversion - Request has Invalid Input - Please verify agaist Service Documentation ");
            response["authoritativeResponse"]["ERROR"] = `Request has Invalid Data .. Please verify agaist Service Documentation`
        }
    } catch (error) {
        logger.error(`Exception - Volume Conversion ${error}`);
        response["authoritativeResponse"]["EXCEPTION"] = `Exception - Volume Conversion - Please reach out to service owners`;
    }

    return response;
}

export { doTempConversion, doVolumeConversion }