const volumeConvSample = {
    "inputValue": 45,
    "inputUnit": "l",
    "outputUnit": "Tbs",
    "studentResponse": "55.0"
}

const tempConvSample = {
    "inputValue": 300,
	"inputUnit":"K",
	"outputUnit":"C",
	"studentResponse":"26.9"
}

export { volumeConvSample, tempConvSample }