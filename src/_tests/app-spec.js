import chai from 'chai';
import chaihttp from 'chai-http';
import app from '../index.js';

//Assertion Style
chai.should();

chai.use(chaihttp);

describe('Test UnitConv API', () => {

    /**
     * Test the Health route
     */
        describe("GET Health Route" , () =>{
            it('It Should return 200', (done) =>{
                chai.request(app).get("/api/v1/health").end((err, response) =>{
                    console.log(response.status);
                    response.should.have.status(200);
                    console.log(JSON.stringify(response));
                    response.text.should.be.eql('Health Check - Complete');
                    done();
                })
            });
        });

});
