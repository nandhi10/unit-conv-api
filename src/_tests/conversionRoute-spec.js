import chai from 'chai';
import chaihttp from 'chai-http';
import app from '../index.js';
import * as testData from './testdata-spec.js';

//Assertion Style
chai.should();

chai.use(chaihttp);



describe('POST Test UnitConv API', () => {

    /**
     * Test for volumeConv Route
     */
    describe("POST Volume Route", () => {
        it('It Should return 200', (done) => {
            chai.request(app).post("/api/v1/unitConversion/volume").send(testData.volumeConvSample).end((err, response) => {
                console.log(response.status);
                response.should.have.status(200);
                console.log(JSON.stringify(response));
                response.text.should.match(/(Student Response)/);
                done();
            })
        });
    });
    /**
     * Test for TempConv Route
     */
    describe("POST TEMP Route", () => {
        it('It Should return 200', (done) => {
            chai.request(app).post("/api/v1/unitConversion/temperature").send(testData.tempConvSample).end((err, response) => {
                console.log(response.status);
                response.should.have.status(200);
                console.log(JSON.stringify(response));
                response.text.should.match(/(Student Response)/);
                done();
            })
        });
    });

});
