import unitConv from "convert-units";

const convertUnit = (requestBody) => {
  return unitConv(requestBody.inputValue)
    .from(requestBody.inputUnit)
    .to(requestBody.outputUnit);
};

export { convertUnit };
